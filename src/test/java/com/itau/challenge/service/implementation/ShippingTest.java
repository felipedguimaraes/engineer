package com.itau.challenge.service.implementation;

import static org.junit.Assert.assertEquals;

import com.itau.challenge.builders.CartBuilder;
import com.itau.challenge.builders.OrderBuilder;
import com.itau.challenge.builders.ProductBuilder;
import com.itau.challenge.builders.PromotionBuilder;
import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Order;
import com.itau.challenge.exceptions.ShippingPriceException;
import com.itau.challenge.service.interfaces.CartService;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Test;

public class ShippingTest {
  private CartService cartService = new CartServiceImpl();

  @Test
  public void shouldCalculateShipping() throws ShippingPriceException {
    var book = ProductBuilder.BOOK();
    var  magazine = ProductBuilder.MAGAZINE();

    Cart cart = CartBuilder.WITH_CUPOM(List.of(book, magazine), "COUPON");
    Order order = OrderBuilder.VALID(cart);

    cartService.calculateShipping(order);

    assertEquals(new BigDecimal("21.0"), order.getShipping().getPrice());
    assertEquals(15, order.getShipping().getDeliveryDeadline());
  }

  @Test
  public void shouldNotCalculateShipping() throws ShippingPriceException {
    var movie = ProductBuilder.MOVIE();
    Cart cart = CartBuilder.WITHOUT_CUPOM(List.of(movie));
    Order order = OrderBuilder.VALID(cart);

    cartService.calculateShipping(order);

    assertEquals(new BigDecimal("0.0"), order.getShipping().getPrice());
    assertEquals(0, order.getShipping().getDeliveryDeadline());
  }
}
