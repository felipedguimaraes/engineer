package com.itau.challenge.service.implementation;

import static org.junit.Assert.assertEquals;

import com.itau.challenge.builders.CartBuilder;
import com.itau.challenge.builders.OrderBuilder;
import com.itau.challenge.builders.ProductBuilder;
import com.itau.challenge.builders.PromotionBuilder;
import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Order;
import com.itau.challenge.domain.Promotion;
import com.itau.challenge.service.interfaces.CartService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import org.junit.Test;

public class PromotionTest {

  private CartService cartService = new CartServiceImpl();

  @Test
  public void shouldApplyPromotionWithValidCoupon() {
    var book = ProductBuilder.BOOK();
    var  magazine = ProductBuilder.MAGAZINE();

    Cart cart = CartBuilder.WITH_CUPOM(List.of(book, magazine), "COUPON");
    List<Promotion> promotions = List.of(PromotionBuilder.WITH_CUPON(Set.of("SKU1")));
    Order order = OrderBuilder.WITH_PROMOTIONS(cart, promotions);

    cartService.calculateDiscount(order);

    assertEquals(new BigDecimal(30), order.getCart().getProducts().get(0).getFinalPrice());
    assertEquals(new BigDecimal(20), order.getCart().getProducts().get(1).getFinalPrice());
  }

  @Test
  public void shouldNotApplyPromotionForHavingNotPromotion() {
    var book = ProductBuilder.BOOK();
    var  magazine = ProductBuilder.MAGAZINE();

    Cart cart = CartBuilder.WITH_CUPOM(List.of(book, magazine), "COUPON");
    List<Promotion> promotions = List.of(PromotionBuilder.WITHOUT_CUPON(Set.of("SKU1")));
    Order order = OrderBuilder.WITH_PROMOTIONS(cart, promotions);

    cartService.calculateDiscount(order);

    assertEquals(new BigDecimal(40), order.getCart().getProducts().get(0).getFinalPrice());
    assertEquals(new BigDecimal(20), order.getCart().getProducts().get(1).getFinalPrice());
  }

  @Test
  public void shouldNotApplyPromotionForNotHavingCouponInCart() {
    var book = ProductBuilder.BOOK();
    var  magazine = ProductBuilder.MAGAZINE();

    Cart cart = CartBuilder.WITHOUT_CUPOM(List.of(book, magazine));
    List<Promotion> promotions = List.of(PromotionBuilder.WITH_CUPON(Set.of("SKU1")));
    Order order = OrderBuilder.WITH_PROMOTIONS(cart, promotions);

    cartService.calculateDiscount(order);

    assertEquals(new BigDecimal(40), order.getCart().getProducts().get(0).getFinalPrice());
    assertEquals(new BigDecimal(20), order.getCart().getProducts().get(1).getFinalPrice());
  }

  @Test
  public void shouldNotApplyPromotionForHavingWrongCouponCouponInCart() {
    var book = ProductBuilder.BOOK();
    var  magazine = ProductBuilder.MAGAZINE();

    Cart cart = CartBuilder.WITH_CUPOM(List.of(book, magazine), "CUPOMMM");
    List<Promotion> promotions = List.of(PromotionBuilder.WITH_CUPON(Set.of("SKU1")));
    Order order = OrderBuilder.WITH_PROMOTIONS(cart, promotions);

    cartService.calculateDiscount(order);

    assertEquals(new BigDecimal(40), order.getCart().getProducts().get(0).getFinalPrice());
    assertEquals(new BigDecimal(20), order.getCart().getProducts().get(1).getFinalPrice());
  }
}
