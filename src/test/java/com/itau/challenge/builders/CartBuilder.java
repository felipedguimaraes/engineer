package com.itau.challenge.builders;

import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Product;
import java.util.List;

public class CartBuilder {
  public static Cart WITHOUT_CUPOM(List<Product> products) {
    return Cart.builder()
        .products(products)
        .build();
  }

  public static Cart WITH_CUPOM(List<Product> products, String coupon) {
    return Cart.builder()
        .products(products)
        .coupon(coupon)
        .build();
  }

}
