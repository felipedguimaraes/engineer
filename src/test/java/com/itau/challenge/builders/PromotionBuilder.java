package com.itau.challenge.builders;

import com.itau.challenge.domain.Promotion;
import java.util.Set;

public class PromotionBuilder {

  public static Promotion WITHOUT_CUPON(Set<String> productSkus){
    return Promotion.builder().productsSkus(productSkus).coupon(null).build();
  }

  public static Promotion WITH_CUPON(Set<String> productSkus){
    return Promotion.builder().productsSkus(productSkus).coupon("COUPON").build();
  }



}
