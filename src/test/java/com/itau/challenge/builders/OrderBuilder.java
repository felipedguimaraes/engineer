package com.itau.challenge.builders;

import com.itau.challenge.domain.Address;
import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Order;
import com.itau.challenge.domain.Promotion;
import com.itau.challenge.domain.Shipping;
import java.util.List;

public class OrderBuilder {
  public static Order VALID(Cart cart) {
    return Order.builder()
        .cart(cart)
        .address(
            Address.builder()
                .cep("05860-000")
                .street("Street")
                .number("350")
                .neighborhood("Some Neiborhood")
                .city("São Paulo")
                .state("SP")
                .build())
        .shipping(Shipping.builder().build())
        .build();
  }

  public static Order WITH_PROMOTIONS(Cart cart, List<Promotion> promotions) {
    return Order.builder().promotions(promotions).cart(cart).build();
  }
}
