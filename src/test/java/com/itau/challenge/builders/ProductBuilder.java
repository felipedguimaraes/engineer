package com.itau.challenge.builders;

import static com.itau.challenge.domain.enums.CategoryEnum.BOOKS;
import static com.itau.challenge.domain.enums.CategoryEnum.E_READER;
import static com.itau.challenge.domain.enums.CategoryEnum.MOVIES;
import static com.itau.challenge.domain.enums.CategoryEnum.TV_SHOW;

import com.itau.challenge.domain.Product;
import java.math.BigDecimal;

public class ProductBuilder {

  public static Product BOOK() {
    return Product.builder()
        .name("A Song of Ice and Fire")
        .categoryEnum(BOOKS)
        .price(new BigDecimal("40"))
        .weight(0.4)
        .quantity(1)
        .sku("SKU1")
        .build();
  }

  public static Product MAGAZINE() {
    return Product.builder()
        .name("The Old Man And The Sea")
        .categoryEnum(E_READER)
        .price(new BigDecimal("20"))
        .weight(0.2)
        .quantity(1)
        .sku("SKU2")
        .build();
  }

  public static Product MOVIE(){
    return Product.builder()
        .name("Iron Man")
        .categoryEnum(MOVIES)
        .price(new BigDecimal("10"))
        .quantity(1)
        .sku("SKU3")
        .build();
  }

  public static Product TV_SHOW(){
    return Product.builder()
        .name("Iron Man")
        .categoryEnum(TV_SHOW)
        .price(new BigDecimal("5"))
        .quantity(1)
        .sku("SKU4")
        .build();
  }
}
