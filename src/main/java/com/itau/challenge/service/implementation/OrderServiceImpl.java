package com.itau.challenge.service.implementation;

import com.itau.challenge.domain.Order;
import com.itau.challenge.service.interfaces.OrderService;
import java.math.BigDecimal;

public class OrderServiceImpl implements OrderService {

  @Override
  public void calculateOrderPrice(Order order) {
    order.setOrderValue(
        order.getShipping().getPrice().add(new BigDecimal(0).add(order.getCart().getPrice())));
  }
}
