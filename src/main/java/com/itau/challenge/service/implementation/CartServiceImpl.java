package com.itau.challenge.service.implementation;

import com.itau.challenge.domain.Order;
import com.itau.challenge.domain.Promotion;
import com.itau.challenge.exceptions.ShippingPriceException;
import com.itau.challenge.service.interfaces.CartService;
import com.itau.challenge.service.predicate.Predicates;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

public class CartServiceImpl implements CartService {

  private static final int TAX_PRICE = 2;
  private static final int DISCOUNT = 10;
  private ShippingServiceImpl shippingService = new ShippingServiceImpl();
  private OrderServiceImpl orderService = new OrderServiceImpl();

  @Override
  public void calculateFinalPrice(Order order) {
    order.getCart().setPrice(new BigDecimal(0));
    order
        .getCart()
        .getProducts()
        .forEach(
            product ->
                order.getCart().setPrice(order.getCart().getPrice().add(product.getFinalPrice())));

    orderService.calculateOrderPrice(order);
  }

  @Override
  public void calculateDiscount(Order order) {
    var promotions =
        order.getPromotions().stream()
            .filter(
                promo ->
                    promo.hasCoupon()
                        && order.getCart().getCoupon() != null
                            && promo.getCoupon().contains(order.getCart().getCoupon()))
            .max(Comparator.comparing(Promotion::getCoupon));
    promotions.ifPresent(
        promo -> {
          order
              .getCart()
              .getProducts()
              .forEach(
                  product -> {
                    if (promo.getProductsSkus().contains(product.getSku())) {
                      product.setDiscount(DISCOUNT);
                    }
                  });
        });
  }

  @Override
  public void calculateTax(Order order) {
    order.getCart().getProducts().stream()
        .filter(Predicates.haveTax())
        .forEach(
            product -> {
              product.setTaxValue(
                  product.getPrice().multiply(BigDecimal.valueOf(TAX_PRICE)).longValue());
            });
  }

  @Override
  public void calculateShipping(Order order) throws ShippingPriceException {
    var freighteableProducts =
        order.getCart().getProducts().stream()
            .filter(Predicates.IsFreeShipping())
            .collect(Collectors.toList());
    if (freighteableProducts.isEmpty()) return;
    shippingService.calculateShipping(order);
  }
}
