package com.itau.challenge.service.implementation;

import com.itau.challenge.domain.Order;
import com.itau.challenge.exceptions.ShippingPriceException;
import com.itau.challenge.service.interfaces.CartService;
import com.itau.challenge.service.interfaces.ShippingService;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class ShippingServiceImpl implements ShippingService {

  private static final Logger LOGGER = Logger.getLogger(CartService.class.getName());

  @Override
  public void calculateShipping(Order order) throws ShippingPriceException {
    StringBuilder url = new StringBuilder();
    var zipCode = order.getAddress().getCep();
    var weight = order.calculateTotalWeight();
    url.append("http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?sCepOrigem=04094-050");
    url.append("&sCepDestino=");
    url.append(zipCode);
    url.append("&nVlPeso=");
    url.append(weight);
    url.append(
        "&nVlComprimento=30&nVlAltura=2&nVlLargura=15&nVlDiametro=0.0&nCdFormato=1"
            + "&sCdMaoPropria=N&sCdAvisoRecebimento=N&nVlValorDeclarado=0,00&nCdServico=04510"
            + "&nCdEmpresa=&sDsSenha=&StrRetorno=xml");
    HttpURLConnection connection;
    try {
      var connectionUrl = new URL(url.toString());
      connection = (HttpURLConnection) connectionUrl.openConnection();
      var result = connection.getInputStream();
      var document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(result);
      var value = document.getDocumentElement().getElementsByTagName("Valor");
      var deadLine = document.getDocumentElement().getElementsByTagName("PrazoEntrega");

      var eValue = (Element) value.item(0);
      var eDeadLine = (Element) deadLine.item(0);

      order
          .getShipping()
          .setPrice(
              BigDecimal.valueOf(Double.parseDouble(eValue.getTextContent()
                  .replace(",", "."))));
      order.getShipping().setDeliveryDeadline(Short.parseShort(eDeadLine.getTextContent()));
    } catch (IOException | ParserConfigurationException | SAXException e) {
      throw new ShippingPriceException("error while calculating shipping price");
    }
  }
}
