package com.itau.challenge.service.implementation;

import com.itau.challenge.domain.Order;
import com.itau.challenge.service.interfaces.EmailService;

public class EmailServiceImpl implements EmailService {

  @Override
  public void sendEmail(Order order) {
    System.out.println(
        String.format(
            "%s, thanks for shopping! See your products bellow:",
            order.getCustomer().getName()));
    order.getCart().getProducts().forEach(System.out::println);

    System.out.println(String.format("Total Price: R$ %s", order.getOrderValue()));
  }
}
