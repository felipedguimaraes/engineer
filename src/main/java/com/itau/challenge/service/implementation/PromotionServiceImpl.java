package com.itau.challenge.service.implementation;

import com.itau.challenge.domain.Promotion;
import com.itau.challenge.service.interfaces.PromotionService;
import java.util.ArrayList;
import java.util.List;

public class PromotionServiceImpl implements PromotionService {

  List<Promotion> promotions = new ArrayList<>();

  @Override
  public void addPromotion(Promotion promotion) {
    promotions.add(promotion);
  }

  @Override
  public List<Promotion> getAll() {
    return promotions;
  }
}
