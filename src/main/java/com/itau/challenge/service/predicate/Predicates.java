package com.itau.challenge.service.predicate;

import com.itau.challenge.domain.Product;
import java.util.function.Predicate;

public class Predicates {

   public static Predicate<Product> haveTax() {
    return product -> !product.getCategoryEnum().isFreeTax();
  }

  public static Predicate<Product> IsFreeShipping() {
    return product -> !product.getCategoryEnum().isDigitalMedia();
  }
}
