package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Product;
import java.util.List;

public interface TaxService {
  List<Product> calculateTax(List<Product> products);
}
