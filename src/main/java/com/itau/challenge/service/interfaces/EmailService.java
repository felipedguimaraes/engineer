package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Order;

public interface EmailService {
  void sendEmail(Order order);
}
