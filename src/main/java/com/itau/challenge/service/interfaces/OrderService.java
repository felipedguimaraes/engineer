package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Order;

public interface OrderService {

  void calculateOrderPrice(Order order);

}
