package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Order;
import com.itau.challenge.domain.Shipping;
import com.itau.challenge.exceptions.ShippingPriceException;

public interface ShippingService {
  void calculateShipping(Order order) throws ShippingPriceException;
}
