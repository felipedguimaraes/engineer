package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Promotion;
import java.util.List;

public interface PromotionService {

  void addPromotion(Promotion promotion);
  List<Promotion> getAll();
}
