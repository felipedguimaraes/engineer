package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Order;
import com.itau.challenge.domain.Product;
import com.itau.challenge.domain.Promotion;
import com.itau.challenge.domain.Shipping;
import com.itau.challenge.exceptions.ShippingPriceException;
import java.util.List;

public interface CartService {

  void calculateFinalPrice(Order order);

  void calculateDiscount(Order order);

  void calculateTax(Order order);

  void calculateShipping(Order order) throws ShippingPriceException;
}
