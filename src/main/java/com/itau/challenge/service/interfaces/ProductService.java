package com.itau.challenge.service.interfaces;

import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Product;

public interface ProductService {

  void calculateProductPrice(Cart cart, Product product);
}
