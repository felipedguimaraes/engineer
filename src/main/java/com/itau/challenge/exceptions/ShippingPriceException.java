package com.itau.challenge.exceptions;

public class ShippingPriceException extends Exception{
  public ShippingPriceException(String message) {
    super(message);
  }
}
