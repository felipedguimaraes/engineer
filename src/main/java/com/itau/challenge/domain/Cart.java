package com.itau.challenge.domain;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Cart {
  private BigDecimal price;
  private List<Product> products;
  private String coupon;
}
