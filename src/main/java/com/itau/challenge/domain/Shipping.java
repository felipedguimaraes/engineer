package com.itau.challenge.domain;

import com.itau.challenge.domain.enums.ShippingCode;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Shipping {
  private double weight;
  private BigDecimal price;
  private short deliveryDeadline;
  private ShippingCode shippingCode;
}
