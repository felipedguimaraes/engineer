package com.itau.challenge.domain.enums;

public enum ShippingCode {
  CODE_04510("04510", "PAC sem contrato"),
  CODE_04014("04014", "SEDEX sem contrato");

  private String code;
  private String name;

  ShippingCode(String code, String name){
    this.code = code;
    this.name = name;
  }
}
