package com.itau.challenge.domain.enums;

public enum CategoryEnum {
  BOOKS("Books", true, false),
  NEWSPAPER("Newspaper", true, false),
  MAGAZINE("Magazine", true, false),
  E_READER("E-reader", true, false),
  MOVIES("Movies", false, true),
  TV_SHOW("Tv Show", false, true),
  E_BOOK("E-books", false, true),
  SONG("CD", false, true);

  private boolean isFreeTax;
  private boolean isDigitalMedia;

  CategoryEnum(final String description) {}

  CategoryEnum(final String description, final boolean isFreeTax, final boolean isDigitalMedia) {
    this(description);
    this.isFreeTax = isFreeTax;
  }

  public boolean isFreeTax() {
    return isFreeTax;
  }

  public boolean isDigitalMedia() {
    return isDigitalMedia;
  }
}
