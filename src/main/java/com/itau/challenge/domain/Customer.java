package com.itau.challenge.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Customer {
  private String name;
  private String email;
  private Address address;
}
