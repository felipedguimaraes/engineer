package com.itau.challenge.domain;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Address {
  private String street;
  private String number;
  private String cep;
  private String neighborhood;
  private String state;
  private String city;
}
