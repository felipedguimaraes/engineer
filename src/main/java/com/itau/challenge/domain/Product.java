package com.itau.challenge.domain;

import com.itau.challenge.domain.enums.CategoryEnum;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {
  private int productId;
  private String sku;
  private String name;
  private BigDecimal price;
  private CategoryEnum categoryEnum;
  private Integer quantity;
  private long discount;
  private long taxValue;
  private double weight = 0;

  public BigDecimal getFinalPrice() {
    return this.price
        .multiply(new BigDecimal(quantity))
        .subtract(BigDecimal.valueOf(discount))
        .add(BigDecimal.valueOf(taxValue));
  }

}
