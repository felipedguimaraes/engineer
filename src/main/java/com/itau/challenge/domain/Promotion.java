package com.itau.challenge.domain;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class Promotion {
  private final String name;
  private final String coupon;
  private final Set<String> productsSkus;

  public boolean hasCoupon() {
    return coupon != null && (coupon.length() > 0);
  }
}
