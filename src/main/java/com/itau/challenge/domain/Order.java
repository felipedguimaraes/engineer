package com.itau.challenge.domain;

import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Order {
  private final Address address;
  private final List<Promotion> promotions;
  private final Customer customer;
  private final Cart cart;
  private BigDecimal tax;
  private Shipping shipping;
  private BigDecimal orderValue;

  public BigDecimal calculateTotalOrder() {
    return this.cart.getProducts().stream()
        .map(Product::getFinalPrice)
        .reduce(new BigDecimal(0), BigDecimal::add)
        .add(shipping.getPrice())
        .add(tax);
  }

  public double calculateTotalWeight() {
    return cart.getProducts().stream().mapToDouble(Product::getWeight).sum();
  }

}
