import static com.itau.challenge.domain.enums.CategoryEnum.BOOKS;
import static com.itau.challenge.domain.enums.CategoryEnum.E_READER;

import com.itau.challenge.domain.Address;
import com.itau.challenge.domain.Cart;
import com.itau.challenge.domain.Customer;
import com.itau.challenge.domain.Order;
import com.itau.challenge.domain.Product;
import com.itau.challenge.domain.Promotion;
import com.itau.challenge.domain.Shipping;
import com.itau.challenge.exceptions.ShippingPriceException;
import com.itau.challenge.service.implementation.CartServiceImpl;
import com.itau.challenge.service.implementation.EmailServiceImpl;
import com.itau.challenge.service.implementation.PromotionServiceImpl;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class Application {

  private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

  public static void main(String[] args) throws ShippingPriceException {
    var cartService = new CartServiceImpl();
    var emailService = new EmailServiceImpl();
    var promotionService = new PromotionServiceImpl();

    // creating promotions
    var promotion =
        Promotion.builder()
            .coupon("COUPON-10")
            .name("New Promotion")
            .productsSkus(Set.of("SKU1"))
            .build();
    promotionService.addPromotion(promotion);

    // creating customer
    var customer =
        Customer.builder()
            .name("Felipe")
            .email("feliped.guimaraes@hotmail.com")
            .address(
                Address.builder()
                    .cep("05860-000")
                    .street("Street")
                    .number("350")
                    .neighborhood("Some Neiborhood")
                    .city("São Paulo")
                    .state("SP")
                    .build())
            .build();

    var book =
        Product.builder()
            .name("A Song of Ice and Fire")
            .categoryEnum(BOOKS)
            .price(new BigDecimal("40"))
            .quantity(2)
            .sku("SKU1")
            .weight(0.5)
            .build();

    var magazine =
        Product.builder()
            .name("The Old Man And The Sea")
            .categoryEnum(E_READER)
            .price(new BigDecimal("20"))
            .quantity(1)
            .sku("SKU2")
            .weight(0.3)
            .build();

    var products = List.of(book, magazine);
    var cart = Cart.builder().products(products).build();
    var order =
        Order.builder()
            .cart(cart)
            .customer(customer)
            .promotions(promotionService.getAll())
            .address(customer.getAddress())
            .shipping(Shipping.builder().build())
            .build();

    cartService.calculateDiscount(order);

    cartService.calculateTax(order);

    cartService.calculateShipping(order);

    cartService.calculateFinalPrice(order);

    emailService.sendEmail(order);
  }
}
